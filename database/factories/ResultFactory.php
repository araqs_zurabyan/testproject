<?php

namespace Database\Factories;

use App\Models\Member;
use App\Models\Result;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Result>
 */
class ResultFactory extends Factory
{
    protected $model = Result::class;

    public function definition()
    {
        return [
            'member_id' => Member::factory(),
            'milliseconds' => $this->faker->numberBetween(500, 1000),
        ];
    }
}
