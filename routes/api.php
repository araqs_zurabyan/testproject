<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;


Route::post('/save-result', [GameController::class, 'saveResult']);
Route::get('/get-top-results', [GameController::class, 'getTopResults']);