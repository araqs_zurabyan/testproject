<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Result extends Model
{
    use HasFactory;

    protected $fillable=[
        'member_id',
        'milliseconds'
    ];
    public function member():BelongsTo
    {
        return $this->belongsTo(Member::class);
    }

}
