<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetResultRequest;
use App\Http\Requests\ResultRequest;
use App\Models\Member;
use App\Models\Result;
use Illuminate\Http\Request;

/**
 * @OA\Info(
 *     title="Your Game API",
 *     version="1.0.0",
 *     description="API documentation for your game",
 *     @OA\Contact(
 *         email="your.email@example.com"
 *     ),
 *     @OA\License(
 *         name="Your License",
 *         url="http://www.your-license-url.com/"
 *     )
 * )
 */
class GameController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/save-result",
     *     summary="Save a game result",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="milliseconds", type="integer"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Result saved successfully",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error", type="string")
     *         )
     *     ),
     * )
     */
    public function saveResult(Request $request)
    {
        $email = $request->email;
        $milliseconds = $request->milliseconds;

        $member = $email ? Member::firstOrCreate(['email' => $email]) : null;

        Result::create([
            'member_id' => $member ? $member->id : null,
            'milliseconds' => $milliseconds,
        ]);

        return response()->json();
    }


    /**
     * @OA\Get(
     *     path="/api/get-top-results",
     *     summary="Get top game results",
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email address to retrieve self result",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Top results retrieved successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(property="top", type="array", @OA\Items(ref="#/components/schemas/TopResult")),
     *                 @OA\Property(property="self", type="object", ref="#/components/schemas/SelfResult")
     *             )
     *         )
     *     ),
     * )
     */
    public function getTopResults(Request $request)
    {
        $email = $request->email;

        $topResults = Result::whereNotNull('member_id')
            ->orderBy('milliseconds')
            ->take(10)
            ->get(['member_id', 'milliseconds']);

        $selfResult = $email
            ? Member::where('email', $email)->first()->results()->orderBy('milliseconds')->first()
            : null;

        $formattedTopResults = $this->formatResults($topResults);
        $formattedSelfResult = $selfResult ? $this->formatResult($selfResult, 0) : null;

        return response()->json(['data' => ['top' => $formattedTopResults, 'self' => $formattedSelfResult]]);
    }

    /**
     * @OA\Schema(
     *     schema="TopResult",
     *     type="object",
     *     @OA\Property(property="email", type="string"),
     *     @OA\Property(property="place", type="integer"),
     *     @OA\Property(property="milliseconds", type="integer"),
     * )
     */
    private function formatResults($results)
    {
        return $results->map(function ($result, $index) {
            return $this->formatResult($result, $index + 1);
        });
    }

    /**
     * @OA\Schema(
     *     schema="SelfResult",
     *     type="object",
     *     @OA\Property(property="email", type="string"),
     *     @OA\Property(property="place", type="integer"),
     *     @OA\Property(property="milliseconds", type="integer"),
     * )
     */
    private function formatResult($result, $place = null)
    {
        return [
            'email' => $result->member ? $result->member->email : null,
            'place' => $place,
            'milliseconds' => $result->milliseconds,
        ];
    }


}
