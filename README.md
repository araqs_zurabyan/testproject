# Project Name

Test

## Getting Started

These instructions will help you set up and run the project on your local machine.

### Prerequisites

- Docker
- Docker Compose

### Installing

1. Clone the repository:

    ```bash
    git clone https://github.com/yourusername/your-repo.git
    ```

2. Navigate to the project directory:

    ```bash
    cd your-repo
    ```

3. Copy the environment file:

    ```bash
    cp .env.example .env
    ```

4. Configure the environment variables in the `.env` file.

5. Build and run the Docker containers:

    ```bash
    docker-compose up -d
    ```

6. Install project dependencies:

    ```bash
    docker-compose exec app composer install
    ```

7. Run database migrations:

    ```bash
    docker-compose exec app php artisan migrate
    ```

8. Generate application key:

    ```bash
    docker-compose exec app php artisan key:generate
    ```

### Running Tests

To run PHPUnit tests:

```bash
docker-compose exec app php artisan test
