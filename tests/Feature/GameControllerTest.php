<?php


namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\Member;
use App\Models\Result;

class GameControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testSaveResult()
    {
        $randomEmail = 'test' . Str::random(5) . '@example.com';

        $response = $this->post('api/save-result', [
            'email' => $randomEmail,
            'milliseconds' => 500,
        ]);

        $response->assertStatus(200);

    }
}

